import lxml.html
import re
import numpy as np

#file_name = './punkte_anon.html'
file_name = '/home/balx/Downloads/punkte_anon.html'

file = open(file_name, 'r')
processing_doc = file.read()
file.close()

root = lxml.html.fromstring(processing_doc)
tds = root.xpath('//table[@border="1"]/tr/td')

data = []
for td in tds:
    num_children = len(td.getchildren())
    value = 0.0
    if num_children == 0:
        value = td.text
    else:
        input_field = td.find('input[@type="text"]')
        if input_field != None:
            value = input_field.value
            if value == None:
                value = 0.0
    data.append(value)

#print len(data)
#print data

num_column = 73
raw_final_exam = []
raw_mini_exam = []
filter = '0.0 (0.00%)'

output = open('output.txt', 'w')
index = 0
while index < len(data):
    if index % num_column == 0:
        if data[index+num_column-1] == filter:
            index += num_column
            continue
        else:
            output.write('\n')
    else:
        output.write(', ')
    output.write(str(data[index]))
    
    if index % num_column == num_column-1:
        raw_final_exam.append(data[index])
    if index % num_column == num_column-6:
        raw_mini_exam.append(data[index])
    index += 1

output.close()

###########################################
# Calculate Correlation Coefficient 
# between mini_exam and final_exam
###########################################
pattern = "[-+]?\d*\.\d+|\d+"
final_exam = []
mini_exam = []

corr_output = open('corr_coef.txt', 'w')
corr_output.write("Mini Exam\tFinal Exam\n")
for i in range(len(raw_final_exam)):
    final = float(re.findall(pattern, raw_final_exam[i])[1])
    final_exam.append(final)
    mini = float(re.findall(pattern, raw_mini_exam[i])[1])
    mini_exam.append(mini)
    corr_output.write("%.2f \t\t %.2f\n" % (mini, final))

corr_coef = np.corrcoef(mini_exam, final_exam)

corr_output.write("\nCorrelation Coefficient:\n")
corr_output.write(str(corr_coef))
corr_output.close()
